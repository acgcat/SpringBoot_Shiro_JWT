package com.jwt.demo.web;

import com.jwt.demo.dao.Msg;
import com.jwt.demo.jwt.JWTUtil;
import com.jwt.demo.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;

@RestController
@Api(value = "测试Web")
public class UserWeb {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "用户登录", notes = "登录--不进行拦截")
    @PostMapping("/login")
    public Msg login(@RequestParam("username") String username,
                     @RequestParam("password") String password) {
        String realPassword = userService.getPassword(username);
        System.out.println(realPassword);
        if (realPassword == null) {
            return Msg.fail().add("info","用户名错误");
        } else if (!realPassword.equals(password)) {
            return Msg.fail().add("info","密码错误");
        } else {
            return Msg.success().add("token", JWTUtil.createToken(username));
        }
    }
    @ApiOperation(value = "无权限", notes = "无权限跳转的接口")
    @RequestMapping(path = "/unauthorized/{message}")
    public Msg unauthorized(@PathVariable String message) throws UnsupportedEncodingException {
        return Msg.fail().add("info",message);
    }
    //admin或user或vip权限的可访问
    @ApiOperation(value = "特定用户访问", notes = "拥有 user, admin 角色的用户可以访问下面的页面")
    @PostMapping("/getMessage")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})//角色
    public Msg getMessage() {
        return Msg.success().add("info","成功获得信息！");
    }

    //admin或vip权限的可访问
    @ApiOperation(value = "Vip用户访问", notes = "拥有 vip 权限可以访问该页面")
    @PostMapping("/getVipMessage")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})//角色
    @RequiresPermissions("vip")//权限
    public Msg getVipMessage() {
        return Msg.success().add("info","成功获得 vip 信息！");
    }
    //admin权限的可访问
    @ApiOperation(value = "admin用户访问", notes = "拥有 admin 权限可以访问该页面")
    @PostMapping("/getAdminMessage")
    @RequiresRoles(logical = Logical.OR, value = {"admin"})//角色
    public Msg getAdminMessage() {
        return Msg.success().add("info","成功获得 管理员 信息！");
    }

}