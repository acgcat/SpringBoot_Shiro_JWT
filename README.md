##  SpringBoot+Shiro+JWT+角色权限管理

#### 介绍
架构demo,maven管理

#### 软件架构
spring boot,mybatis,mysql,shiro,jwt,swagger2,

#### 使用说明
启动访问:ip:port/swagger-ui.html(即可测试代码,token测试需其他工具)

源码仅供学习参考
